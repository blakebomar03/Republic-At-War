# Republic-At-War
## About this Repository
This is the official repository of the mod "Republic at War" for the game "Star Wars Empire at War - Forces of Corruption". 

This repository is organized in different branches which are named equal to the public mod versions. Each branch contains the new and modified files foerech version
as well as a scheme that is used by the launcher. 

The master branch does not hold any mod files but contains information required by the launcher. Also this branch hosts updates for the launcher itself.

Based on the content published here everyone is welcome to share new ideas, report bugs and commit pull requests. 
If you are unfamiliar with Git we would suggest to read [How to post issues](https://gitlab.com/Republic-at-War/Republic-At-War/wikis/home#how-to-post-issues) first.

Beside viewing all the files of Republic at War you have the option to read through the [Wiki](https://gitlab.com/Republic-at-War/Republic-At-War/wikis/home). The Wiki contains many
additional information about the mod, including news articles, a small development blog and a FAQ.


## What this Repository is NOT
We will not upload the entire mod here. All branches will only hold the 'difference versions'. 
The full mod will avaiable through [Moddb.com](http://www.moddb.com/mods/republic-at-war).
A detailed insturction how to get the mod can be reviewed in the [Wiki](https://gitlab.com/Republic-at-War/Republic-At-War/wikis/home).



